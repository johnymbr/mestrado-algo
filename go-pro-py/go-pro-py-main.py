import sys
import subprocess
import gopropy.join_files as join


def main(go_pro_video_path, video_name):
    """
    to generate the go pro file using go program.
    :param go_pro_video_path:
    :param video_name:
    :return:
    """
    command = 'ffmpeg -y -i {0}/{1}.MP4 -codec copy -map 0:3:handler_name:\"        GoPro MET\" -f rawvideo {0}/{1}.bin' \
        .format(go_pro_video_path, video_name)
    subprocess.call(command, shell=True)

    # command = './gpmdinfo -i {0}/{1}.bin -p {0}'.format(go_pro_video_path, video_name)
    # subprocess.call(command, shell=True)

    # join.join_files(join.GoProFiles(go_pro_video_path))


if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])
