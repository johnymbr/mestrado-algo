import numpy as np
from scipy import interpolate, signal
import peakutils
import matplotlib.pyplot as plt
import pandas as pd


class GoProFiles:
    def __init__(self, path):
        """
        check if path exists and load files: gps, gyro.
        :param path:
        """
        self.path = path
        self.gp = np.loadtxt(path + '/gps.csv', delimiter=',', skiprows=1)
        self.gy = np.loadtxt(path + '/gyro.csv', delimiter=',', skiprows=1)
        self.ac = np.loadtxt(path + '/accl.csv', delimiter=',', skiprows=1)


def convert_gps_time(go_pro_file):
    """
    Convert the GPS given time from seconds to miliseconds
    adding the decimal places
    """
    first_iteration = False
    first_value = None
    for i in range(go_pro_file.gp.shape[0]):

        if not first_iteration:
            first_value = pd.to_datetime(go_pro_file.gp[i, 5], unit='us')
            first_iteration = True

        gp_value = pd.to_datetime(go_pro_file.gp[i, 5], unit='us')
        gp_ms_value = int((gp_value - first_value).total_seconds() * 1000)
        go_pro_file.gp[i, 5] = gp_ms_value + (i - 1) * (1 / 55000)


def interpolate_gps_with_gyro(go_pro_file):
    """
    Interpolate the GPS observations to the gyro rate
    """
    gps_gy = np.ndarray(shape=(go_pro_file.gy.shape[0], 5))
    gps_gy[:, 0] = go_pro_file.gy[:, 0]
    f = interpolate.interp1d(go_pro_file.gp[:, 5], go_pro_file.gp[:, 0], fill_value='extrapolate')
    gps_gy[:, 1] = f(go_pro_file.gy[:, 0])
    f = interpolate.interp1d(go_pro_file.gp[:, 5], go_pro_file.gp[:, 1], fill_value='extrapolate')
    gps_gy[:, 2] = f(go_pro_file.gy[:, 0])
    f = interpolate.interp1d(go_pro_file.gp[:, 5], go_pro_file.gp[:, 2], fill_value='extrapolate')
    gps_gy[:, 3] = f(go_pro_file.gy[:, 0])
    f = interpolate.interp1d(go_pro_file.gp[:, 5], go_pro_file.gp[:, 3], fill_value='extrapolate')
    gps_gy[:, 4] = f(go_pro_file.gy[:, 0])

    accl_gy = np.ndarray(shape=(go_pro_file.gy.shape[0], 5))
    accl_gy[:, 0] = go_pro_file.gy[:, 0]
    f = interpolate.interp1d(go_pro_file.ac[:, 0], go_pro_file.ac[:, 1], fill_value='extrapolate')
    accl_gy[:, 1] = f(go_pro_file.gy[:, 0])
    f = interpolate.interp1d(go_pro_file.ac[:, 0], go_pro_file.ac[:, 2], fill_value='extrapolate')
    accl_gy[:, 2] = f(go_pro_file.gy[:, 0])
    f = interpolate.interp1d(go_pro_file.ac[:, 0], go_pro_file.ac[:, 3], fill_value='extrapolate')
    accl_gy[:, 3] = f(go_pro_file.gy[:, 0])

    gps_gy_accl = np.concatenate((gps_gy, accl_gy[:, 1:]), axis=1)
    gps_gy_accl = np.concatenate((gps_gy_accl, go_pro_file.gy[:, 1:]), axis=1)

    save_gps_info(go_pro_file, '/before_median.csv', gps_gy_accl)

    return gps_gy_accl


def peak_finding(go_pro_file):
    res = interpolate_gps_with_gyro(go_pro_file)
    np.savetxt(go_pro_file.path + '/gps_accl_gy.csv', res, fmt='%.10f', delimiter=',', )

    indexes = peakutils.indexes(res[:, 10], thres=0.65, min_dist=200)
    plt.plot(res[:, 0], res[:, 10])
    plt.plot(res[:, 0][indexes], res[:, 10][indexes], marker='o', ls='')
    plt.show()
    peak_info = np.ndarray(shape=(len(indexes), 3))
    peak_info[:, 0] = res[:, 0][indexes]
    peak_info[:, 1] = res[:, 1][indexes]
    peak_info[:, 2] = res[:, 2][indexes]
    return peak_info


def handle_aux_matrix(aux_matrix, result_matrix):
    if aux_matrix is not None:
        f_value = None
        l_value = None
        accl_med = 0
        accl_y_med = 0
        accl_z_med = 0
        speed_med = 0
        highest_gy_idx = 0
        lowest_gy_idx = 0
        for j in range(aux_matrix.shape[0]):
            if f_value is None:
                f_value = aux_matrix[j, 0]

            l_value = aux_matrix[j, 0]
            accl_med = accl_med + aux_matrix[j, 5]
            accl_y_med = accl_y_med + aux_matrix[j, 6]
            accl_z_med = accl_z_med + aux_matrix[j, 7]
            speed_med = speed_med + aux_matrix[j, 4]

            if aux_matrix[j, 10] > aux_matrix[highest_gy_idx, 10]:
                highest_gy_idx = j

            if aux_matrix[j, 10] < aux_matrix[lowest_gy_idx, 10]:
                lowest_gy_idx = j

        time = l_value - f_value
        accl_med = accl_med / aux_matrix.shape[0]
        accl_y_med = accl_y_med / aux_matrix.shape[0]
        accl_z_med = accl_z_med / aux_matrix.shape[0]
        speed_med = speed_med / aux_matrix.shape[0]
        lat = aux_matrix[highest_gy_idx, 1]
        long = aux_matrix[highest_gy_idx, 2]
        peak = aux_matrix[highest_gy_idx, 10]
        lowest = aux_matrix[lowest_gy_idx, 10]
        ms_init = np.min(aux_matrix[:, 0])
        ms_final = np.max(aux_matrix[:, 0])
        ms = aux_matrix[highest_gy_idx, 0]

        if result_matrix is None:
            result_matrix = np.array([[peak, time, accl_med, accl_y_med, accl_z_med, speed_med, lat, long, ms_init, ms_final, ms]])
        else:
            result_matrix = np.concatenate((result_matrix, [[peak, time, accl_med, accl_y_med, accl_z_med, speed_med, lat, long, ms_init, ms_final, ms]]))

        aux_matrix = None

    return aux_matrix, result_matrix


def apply_median_filter(go_pro_file, gps_gyro_accl):
    aux_matrix = None
    size_m = gps_gyro_accl.shape[0]
    window = 100
    for i in range(gps_gyro_accl.shape[0]):
        if size_m > (i + window):
            gyroy = np.median(gps_gyro_accl[i:(i + window), 10])
            accly = np.median(gps_gyro_accl[i:(i + window), 6])
        else:
            gyroy = np.median(gps_gyro_accl[i:size_m, 10])
            accly = np.median(gps_gyro_accl[i:size_m, 6])
        print('GiroY: {0} and AcclY: {1}'.format(gyroy, accly))
        if gyroy >= -0.6 and accly >= -0.9:
            if aux_matrix is None:
                aux_matrix = np.array([gps_gyro_accl[i]])
            else:
                aux_matrix = np.concatenate((aux_matrix, [gps_gyro_accl[i, :]]))

    save_gps_info(go_pro_file, '/after_median.csv', aux_matrix)

    return aux_matrix


def finding_bumps(go_pro_file, gps_gyro_accl):
    aux_matrix = None
    result_matrix = None
    size_m = gps_gyro_accl.shape[0]
    window = 150
    for i in range(gps_gyro_accl.shape[0]):
        gyroy = gps_gyro_accl[i, 10]
        accly = gps_gyro_accl[i, 6]
        if size_m > (i + window):
            window_max = np.amax(gps_gyro_accl[i:(i + window), 10])
            accly_max = np.amax(gps_gyro_accl[i:(i + window), 6])
        else:
            window_max = np.amax(gps_gyro_accl[i:size_m, 10])
            accly_max = np.amax(gps_gyro_accl[i:size_m, 6])
        print('GiroY: {0} and AcclY: {1}'.format(gyroy, accly))
        if gyroy >= 0.06:  # and accly >= -2.0:
            if aux_matrix is None:
                aux_matrix = np.array([gps_gyro_accl[i]])
            else:
                aux_matrix = np.concatenate((aux_matrix, [gps_gyro_accl[i, :]]))
        else:
            if window_max >= 0.14 and aux_matrix is not None:  # and accly_max >= -2.0:
                aux_matrix = np.concatenate((aux_matrix, [gps_gyro_accl[i, :]]))
            else:
                aux_matrix, result_matrix = handle_aux_matrix(aux_matrix, result_matrix)

    aux_matrix, result_matrix = handle_aux_matrix(aux_matrix, result_matrix)

    np.savetxt(go_pro_file.path + '/result_matrix.csv', result_matrix, fmt='%.10f', delimiter=',', )


def save_gps_info(go_pro_file, file_name, peak_info):
    np.savetxt(go_pro_file.path + file_name, peak_info, fmt='%.10f', delimiter=',', )


def join_files(go_pro_file):
    convert_gps_time(go_pro_file)
    gps_gyro_accl = interpolate_gps_with_gyro(go_pro_file)
    # gps_gyro_accl = apply_median_filter(go_pro_file, gps_gyro_accl)
    finding_bumps(go_pro_file, gps_gyro_accl)
    # save_gps_info(go_pro_file, peak_info)
