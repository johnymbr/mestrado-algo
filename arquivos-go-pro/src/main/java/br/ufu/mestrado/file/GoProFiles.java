package br.ufu.mestrado.file;

import java.io.BufferedReader;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe que ira receber o caminho
 * dos arquivos e ira fazer a juncao dos mesmos.
 */
public class GoProFiles {

    private String path;
    private String[] filenames = {"gps_2.txt", "acel.txt", "gyro.txt"};

    public GoProFiles(String path) {
        this.path = path;
    }

    /**
     * Metodo que ira proceder com a juncao dos arquivos.
     */
    public void join() {
        try (BufferedReader br = Files.newBufferedReader(Paths.get(this.path + File.separator + this.filenames[0]), StandardCharsets.UTF_8)) {
            List<double[]> gpsMatrix = new ArrayList<>();
            for (String line = null; (line = br.readLine()) != null;) {
                System.out.println(line);

                String[] lineSplit = line.split("\t");
                double[] gpsInfo = new double[lineSplit.length];
                for(int i = 0; i < lineSplit.length; i++) {
                    gpsInfo[i] = Double.valueOf(lineSplit[i]);
                }
                gpsMatrix.add(gpsInfo);
            }
        } catch( Exception ex) {
            System.out.println("Error when reading GPS File.");
        }
    }
}
