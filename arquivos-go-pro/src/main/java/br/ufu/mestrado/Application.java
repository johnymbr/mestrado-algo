package br.ufu.mestrado;

import br.ufu.mestrado.file.GoProFiles;

/**
 * Classe principal, que ira iniciar a aplicacao.
 * Inicialmente ira receber o caminho dos arquivos
 * que devem ser "juntados".
 */
public class Application {

    public static void main(String[] args) {
        if (args == null || args.length < 1) {
            throw new IllegalArgumentException("The path for the files must be informed.");
        }
        GoProFiles gpf = new GoProFiles(args[0]);
        gpf.join();
    }
}
